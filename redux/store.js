import { createStore } from 'redux'

const initialState={
    data: {},
    searchText: '',
    totalPrice: 0,
    isLoading: true,
    isError: false
}

const reducer = (state = initialState , action) => {
    switch(action.type){
        case "addFetch":
            console.log(action.dataApi)
            return{...state,
                    data: action.dataApi,
                    isLoading: false,
                    isError: false
            }
        case "errFetch":
            return{...state,
                isLoading: false,
                isErros: false
            }
        case "updatePrice":
            return{...state,
                totalPrice: action.price

            }
        default: 
            return state
    }
}

const store = createStore(reducer)
export { store }