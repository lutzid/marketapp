import React, { Component } from 'react';
import {
    StyleSheet, 
    Text, 
    View, 
    Image, 
    TouchableOpacity, 
    ImageBackground,
    TextInput,
    Button
} from 'react-native';
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";
import bgImage from '../assets/background.png'

export default class LoginScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
          userName: '',
          password: '',
          isError: false,
        }
      }
    
      loginHandler() {
        if(this.state.password === '12345678') {
          this.props.navigation.navigate('Home', {
            userName: this.state.userName
          })
        } else
          this.setState({ isError: true })
      }

    render () {
        return (
            <ImageBackground source={bgImage} style={styles.backgroundContainer}>
                <Text style={styles.pageName}>Sign In</Text>
                <View style={styles.body}>
                    <View style={styles.card}>
                        <View style={styles.inputContainer}>
                            <Text>Email</Text>
                            <TextInput 
                                style={styles.input} 
                                onChangeText={userName => this.setState({ userName })}
                            />
                            <Text>Password</Text>
                            <TextInput 
                                style={styles.input}
                                onChangeText={password => this.setState({ password })}
                                secureTextEntry={true}
                            />
                            <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Password Salah</Text>
                            <TouchableOpacity style={styles.loginButton} onPress={() => this.loginHandler()} >
                                <Text style={styles.login}>Login</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.signUp}>
                        <Text>Don’t have an account? </Text>
                        <Text style={{color: '#2CABF3'}}>Sign Up</Text>
                    </View>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    backgroundContainer: {
        flex: 1,
        width: null,
        height: null,
    },
    pageName: {
        alignItems: 'flex-start',
        fontSize: 24,
        color: '#FFFFFF',
        margin: 20
    },
    body: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    card: {
        backgroundColor: '#FFFFFF',
        height: 400,
        width: 320,
        borderRadius: 16,
        borderRightColor: '#000000',
        elevation: 3
    },
    inputContainer: {
        margin: 30
    },
    input: {
        height: 45,
        borderRadius: 8,
        marginTop: 10,
        marginBottom: 20,
        fontSize: 16,
        backgroundColor: '#FFFFFF',
        borderColor: '#E5E5E5',
        borderWidth: 1
    },
    loginButton: {
        backgroundColor: "#2CABF3",
        borderRadius: 8,
        height: 45,
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    login: {
        fontSize: 18,
        color: '#FFFFFF'
    },
    signUp: {
        flexDirection: 'row'
    },
    errorText: {
      color: 'red',
      textAlign: 'center',
      marginBottom: 16,
    },
    hiddenErrorText: {
      color: 'transparent',
      textAlign: 'center',
      marginBottom: 16,
    }
});