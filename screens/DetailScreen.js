import React, { Component } from 'react';
import {
    StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Dimensions, Button
} from 'react-native';

const DEVICE = Dimensions.get('window')

export default class DetailScreen extends React.Component {
    constructor(props) {
      super(props)
    }
    currencyFormat(num) {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };
    render() {
        let produk = this.props.route.params.item.item
        return (
          <View style={styles.itemContainer}>
            <Image source={{ uri: produk.gambaruri }} style={styles.itemImage} resizeMode='contain' />
            <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{produk.nama}</Text>
            <Text style={styles.itemPrice}>{this.currencyFormat(Number(produk.harga))}</Text>
            <Text style={styles.itemStock}>Sisa stok: {produk.stock}</Text>
          </View>
        );
      }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    headerText: {
      fontSize: 18,
      fontWeight: 'bold'
    },
    itemContainer: {
      width: DEVICE.width,
      backgroundColor: '#FFFFFF',
      justifyContent: 'center',
      alignItems: 'center',
      padding: 10
    },
    itemImage: {
      flex: 1,
      width: 360,
      height: 360
    },
    itemName: {
      fontWeight: 'bold',
      marginBottom: 5
    },
    itemPrice: {
      fontWeight: "bold",
      color: "blue"
    },
    itemStock: {
      fontWeight: '400',
      marginBottom: 3
    },
    itemButton: {
      marginBottom: 5,
      borderRadius: 100,
      fontWeight: '200',
      marginBottom: 3
    },
    detailButton: {
      backgroundColor: '#E5E5E5',
      padding: 5,
      borderRadius: 5
    }
  })