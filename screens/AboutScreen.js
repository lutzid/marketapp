import React, { Component } from 'react';
import {
    StyleSheet, 
    Text, 
    View, 
    Image, 
    TouchableOpacity, 
    ImageBackground,
    TextInput
} from 'react-native';
import bgImage from '../assets/background.png'
import Icon from 'react-native-vector-icons/FontAwesome'
import Icon2 from 'react-native-vector-icons/MaterialIcons'

export default class App extends Component {
    render () {
        return (
            <ImageBackground source={bgImage} style={styles.backgroundContainer}>
                <View style={styles.body}>
                    <View style={styles.profile}>
                    <Image source={{uri: 'https://randomuser.me/api/portraits/men/0.jpg'}} style={{width: 110, height:110, borderRadius: 100, marginBottom: 10}}/>
                        <Text style={{color: '#FFFFFF', fontSize: 24, marginBottom: 30}}>Zidan Lutfi</Text>
                    </View>
                    <View style={styles.card}>
                        <View style={styles.itemContainer}>
                            <View style={styles.item}>
                                <Icon name="instagram" size={60}/>
                                <Text>lut.zidan</Text>
                            </View>
                            <View style={styles.item}>
                                <Icon name="github" size={60}/>
                                <Text>lutzid</Text>
                            </View>
                            <View style={styles.item}>
                                <Icon name="facebook" size={60}/>
                                <Text>zidan.lutfi</Text>
                            </View>
                            <View style={styles.item}>
                                <Icon name="twitter" size={60}/>
                                <Text>luzidan1</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    backgroundContainer: {
        flex: 1,
        width: null,
        height: null,
    },
    profile: {
    },
    body: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    card: {
        backgroundColor: '#FFFFFF',
        height: 400,
        width: 320,
        borderRadius: 16,
        borderRightColor: '#000000',
        elevation: 3
    },
    itemContainer: {
        margin: 40,
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'center'
    },
    item: {
        width: 120,
        alignItems: 'center',
        padding: 10
    }
});