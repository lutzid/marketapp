import * as React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import 'react-native-gesture-handler';
import Icon from "react-native-vector-icons/MaterialIcons";
import { Provider } from 'react-redux';
import { store } from '../redux/store';

import LoginScreen from './LoginScreen';
import HomeScreen from './HomeScreen';
import DetailScreen from './DetailScreen'
import AboutScreen from './AboutScreen'

const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator()

const TabsScreen = (props) => (
  <Tabs.Navigator>
      <Tabs.Screen name='HomeScreen' component={HomeScreen} options={
      {
        tabBarLabel: 'Home',
        tabBarIcon: () => (
          <Icon name="home" size={25} />
        )
      }
    }
    initialParams={ props.route.params }
/>
      <Tabs.Screen name='AboutScreen' component={AboutScreen} options={
      {
        tabBarLabel: 'About Me',
        tabBarIcon: () => (
          <Icon name="supervisor-account" size={25} />
        )
      }
    }
    initialParams={ props.route.params }
/>
  </Tabs.Navigator>
)

export default class App extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Provider store={ store }>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login" >
          <Stack.Screen name='Login' component={LoginScreen} options={{ headerShown: false }} />
          <Stack.Screen name='Home' component={TabsScreen} options={{ headerTitle: 'Menu' }} />
          <Stack.Screen name='Detail' component={DetailScreen} options={{ headerTitle: 'Detail Barang' }} />
        </Stack.Navigator>
      </NavigationContainer>
      </Provider>
    );
  }
}
