import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button, ActivityIndicator } from 'react-native';
import Axios from 'axios';
import {connect} from "react-redux";

const DEVICE = Dimensions.get('window')

class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    // this.state = {
    //   data: {},
    //   searchText: '',
    //   totalPrice: 0,
    //   isLoading: true,
    //   isError: false
    // }
  }

  componentDidMount() {
    this.getProduct()
  }

  getProduct = () => {
      Axios.get(`https://react-native-practice-2d365.firebaseio.com/produk.json`).then(res => {
        console.log(res.data)
        console.log('props', this.props.data)
        this.props.addData(res.data)
      }).catch(err => {
        this.props.errData()
        console.log(err)
      })
    }

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  updatePrice(price) {
    //? #Soal Bonus (10 poin) 
    //? Buatlah teks 'Total Harga' yang akan bertambah setiap kali salah satu barang/item di klik/tekan.
    //? Di sini, buat fungsi untuk menambahkan nilai dari state.totalPrice dan ditampilkan pada 'Total Harga'.
    
    // Kode di sini
    let add = parseInt(price)
    console.log(add)
    let total = this.props.totalPrice + add
    this.props.updatePrice( total)
  }
  goToDetail(produk) {
    this.props.navigation.push("Detail",{item: produk})
  }

  render() {
    //  If load data
    if (this.props.isLoading) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    // If data not fetch
    else if (this.props.isError) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
    // If data finish load
    console.log(this.props.route.params.userName)
    return (
      <View style={styles.container}>
        <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text>Hai,{'\n'}
              <Text style={styles.headerText}>{ this.props.route.params.userName }</Text>
            </Text>

            <Text style={{ textAlign: 'right' }}>Total Harga{'\n'}
              <Text style={styles.headerText}>{ this.currencyFormat(this.props.totalPrice) }</Text>
            </Text>
          </View>
          <View>

          </View>
          <TextInput
            style={{ backgroundColor: 'white', marginTop: 8 }}
            placeholder='Cari barang..'
            onChangeText={(searchText => this.setState({ searchText }))}
          />
        </View>

        <FlatList 
          data={ this.props.data }
          renderItem={ (produk) => {
            return <ListItem produk={ produk.item } addPrice={ () => this.updatePrice(produk.item.harga)} goToDetail={()=>this.goToDetail(produk)}/>}
          }
          keyExtractor={ (produk) => produk.id }
          numColumns ={ 2 }
        />

      </View>
    )
  }
};

class ListItem extends React.Component {
  constructor(props){
    super(props)
  }

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  //? #Soal No 3 (15 poin)
  //? Buatlah styling komponen ListItem, agar dapat tampil dengan baik di device

  render() {
    console.log(this.props)
    const produk = this.props.produk
    return (
      <View style={styles.itemContainer}>
        <Image source={{ uri: produk.gambaruri }} style={styles.itemImage} resizeMode='contain' />
        <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{produk.nama}</Text>
        <Text style={styles.itemPrice}>{this.currencyFormat(Number(produk.harga))}</Text>
        <Text style={styles.itemStock}>Sisa stok: {produk.stock}</Text>
        <Button title='BELI' color='blue' onPress={this.props.addPrice}/>
        <TouchableOpacity style={styles.detailButton} onPress={this.props.goToDetail}>
          <Text>Detail</Text>
        </TouchableOpacity>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold'
  },
  itemContainer: {
    width: DEVICE.width * 0.44,
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10
  },
  itemImage: {
    flex: 1,
    width: 80,
    height: 80
  },
  itemName: {
    fontWeight: 'bold',
    marginBottom: 5
  },
  itemPrice: {
    fontWeight: "bold",
    color: "blue"
  },
  itemStock: {
    fontWeight: '400',
    marginBottom: 3
  },
  itemButton: {
    marginBottom: 5,
    borderRadius: 100,
    fontWeight: '200',
    marginBottom: 3
  },
  detailButton: {
    backgroundColor: '#E5E5E5',
    padding: 5,
    borderRadius: 5
  }
})
const stateProp=(state)=>{
  return{
      data: state.data,
      totalPrice: state.totalPrice,
      isLoading: state.isLoading
  }
}
const actionProp=()=>{
  return {
      addData:(data) => { return { type: 'addFetch', dataApi:data} },
      errData:() => { return { type: 'errFetch' } },
      updatePrice:(price) => { return { type: 'updatePrice', price } }
  }
}
export default connect(stateProp, actionProp())(HomeScreen)
